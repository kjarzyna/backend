<?php

use App\Http\Controllers\Api\ApartmentController;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\FacilityController;
use App\Http\Controllers\Api\HotelController;
use Illuminate\Support\Facades\Route;

Route::get('/apartments', [ApartmentController::class, 'index']);
Route::get('/hotels', [HotelController::class, 'index']);
Route::get('/facilities', [FacilityController::class, 'index']);
Route::get('/cities', [CityController::class, 'index']);
