<?php

namespace Tests\Feature\Api;

use App\Models\Apartment;
use App\Models\City;
use App\Models\Facility;
use App\Models\Hotel;
use App\Service\QueryBuilder\FilterItem;
use App\Service\QueryBuilder\PaginationItem;
use App\Service\QueryBuilder\Query;
use Database\Factories\ApartmentFactory;
use Database\Factories\CityFactory;
use Database\Factories\FacilityFactory;
use Database\Factories\HotelFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ApartmentTest extends TestCase
{
    protected const DEFAULT_HEADERS = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    ];

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndexAction()
    {
        $this->fillDatabase();
        $response = $this->get(sprintf('/api/apartments%s', $this->buildRequestParams()), self::DEFAULT_HEADERS);

        $response
            ->assertJsonStructure([
                'data' => [
                    0 => [
                        'id',
                        'name',
                        'hotel' => [
                            'id',
                            'name'
                        ],
                        'photoUrl',
                        'price',
                        'guestsMaxAmount',
                        'roomArea',
                        'order',
                        'facilities' => [
                            0 => [
                                'id',
                                'name'
                            ]
                        ]
                    ]
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'per_page',
                    'to',
                    'total'
                ]
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    public function testInvalidPerPageNumber()
    {
        $params = $this->buildRequestParams(
            [PaginationItem::KEY_PAGE => 1, PaginationItem::KEY_PER_PAGE => 123]
        );
        $response = $this->get(sprintf('/api/apartments%s', $params), self::DEFAULT_HEADERS);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'meta.perPage' => [
                        'The selected meta.per page is invalid.'
                    ]
                ]
            ]);
    }

    public function testFilterNoResultsForCity()
    {
        $this->fillDatabase();
        $params = $this->buildRequestParams([], [
                [
                    FilterItem::KEY_FIELD => 'hotel.city_id',
                    FilterItem::KEY_TYPE => 'IN',
                    FilterItem::KEY_VALUES => [CityFactory::DEFAULT_ITEMS_COUNT + 1]
                ]
            ]
        );

        $response = $this->get(sprintf('/api/apartments%s', $params), self::DEFAULT_HEADERS);

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [],
                'meta' => [
                    'current_page' => 1,
                    'from' => null,
                    'to' => null,
                    'total' => 0
                ]
            ])
            ->assertJsonMissing(['data' => [0 => []]]);
    }

    public function testQueryParamsRequired()
    {
        $response = $this->get('/api/apartments', self::DEFAULT_HEADERS);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    Query::KEY_FILTERS => [
                        'The filters field must be present.'
                    ],
                    Query::KEY_META => [
                        'The meta field is required.'
                    ],
                    'meta.page' => [
                        'The meta.page field is required.'
                    ],
                    'meta.perPage' => [
                        'The meta.per page field is required.'
                    ]
                ]
            ]);
    }

    /**
     * @param array $meta
     * @param array $filters
     * @return string
     */
    protected function buildRequestParams(array $meta = [], array $filters = []): string
    {
        $params = [];

        $params[Query::KEY_META]    = !empty($meta) ? $meta : [PaginationItem::KEY_PAGE => 1, PaginationItem::KEY_PER_PAGE => 10];
        $params[Query::KEY_FILTERS] = $filters;

        return sprintf('?q=%s', json_encode($params));
    }

    protected function fillDatabase(): void
    {
        City::factory(CityFactory::DEFAULT_ITEMS_COUNT)->create();
        Hotel::factory(HotelFactory::DEFAULT_ITEMS_COUNT)->create();
        Facility::factory(FacilityFactory::DEFAULT_ITEMS_COUNT)->create();
        Apartment::factory(ApartmentFactory::DEFAULT_ITEMS_COUNT)->create()->each(function ($apartment) {
            $facilities = [];
            for ($i = 1; $i <= rand(1, 10); $i++) {
                $facilities[] = rand(1, FacilityFactory::DEFAULT_ITEMS_COUNT);
            }
            $apartment->facilities()->sync($facilities);
        });
    }
}
