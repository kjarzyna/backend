<?php

namespace Tests\Feature\Api;

use App\Models\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class CityTest extends TestCase
{
    use RefreshDatabase;

    /**
     *
     */
    public function testIndexAction()
    {
        City::factory(5)->create();

        $response = $this->get('/api/cities');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
            'data' => [
                0 => [
                    'id', 'name'
                ]
            ]
        ]);
    }
}
