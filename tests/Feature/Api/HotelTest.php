<?php

namespace Tests\Feature\Api;

use App\Models\Hotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class HotelTest extends TestCase
{
    use RefreshDatabase;

    /**
     *
     */
    public function testIndexAction()
    {
        Hotel::factory(5)->create();

        $response = $this->get('/api/hotels');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    0 => [
                        'id', 'name'
                    ]
                ]
            ]);
    }
}
