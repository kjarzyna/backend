<?php

namespace Tests\Feature\Api;

use App\Models\Facility;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class FacilityTest extends TestCase
{
    use RefreshDatabase;

    /**
     *
     */
    public function testIndexAction()
    {
        Facility::factory(5)->create();

        $response = $this->get('/api/facilities');

        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    0 => [
                        'id', 'name'
                    ]
                ]
            ]);
    }
}
