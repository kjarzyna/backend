<?php

namespace App\Http\Responses;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ApiResponse
 * @package App\Http\Responses\
 */
abstract class ApiResponse extends JsonResponse {}
