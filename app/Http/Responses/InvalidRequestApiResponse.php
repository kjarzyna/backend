<?php

namespace App\Http\Responses;

/**
 * Class InvalidRequestApiResponse
 * @package App\Http\Responses
 */
class InvalidRequestApiResponse extends ApiResponse
{
    /**
     * @param array $data
     * @param int $statusCode
     * @return static
     */
    public static function make(array $data = [], int $statusCode = self::HTTP_UNPROCESSABLE_ENTITY): self
    {
        return (new self(
            !empty($data) ? $data : ['Invalid request.'],
            $statusCode
        ));
    }
}
