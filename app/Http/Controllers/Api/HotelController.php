<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\HotelResource;
use App\Http\Responses\ApiResponse;
use App\Models\Hotel;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class HotelController
 * @package App\Http\Controllers\Api
 */
class HotelController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return HotelResource::collection(Hotel::orderBy(Hotel::FIELD_ORDER)->get());

    }
}
