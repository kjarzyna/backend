<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApartmentResource;
use App\Http\Responses\InvalidRequestApiResponse;
use App\Repository\ApartmentRepository;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ApartmentController
 * @package App\Http\Controllers\Api
 */
class ApartmentController extends Controller
{

    /**
     * @var ApartmentRepository
     */
    private ApartmentRepository $apartmentRepository;

    /**
     * ApartmentController constructor.
     * @param ApartmentRepository $apartmentRepository
     */
    public function __construct(ApartmentRepository $apartmentRepository)
    {
        $this->apartmentRepository = $apartmentRepository;
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection|JsonResponse
     * @throws ValidationException
     */
    public function index(Request $request)
    {
        try {
            $data = $this->apartmentRepository->findByRequestQuery($request);
        } catch (QueryException|\BadMethodCallException $e) {
            return InvalidRequestApiResponse::make();
        }

        return ApartmentResource::collection($data);
    }
}
