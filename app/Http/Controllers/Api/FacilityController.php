<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FacilityResource;
use App\Models\Facility;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class FacilityController
 * @package App\Http\Controllers\Api
 */
class FacilityController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return FacilityResource::collection(Facility::all());
    }
}
