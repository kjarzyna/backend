<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'hotel' => [
                'id' => $this->hotel->id,
                'name' => $this->hotel->name
            ],
            'photoUrl' => $this->photo_url,
            'price' => $this->price,
            'guestsMaxAmount' => $this->guests_max_amount,
            'roomArea' => $this->room_area,
            'order' => $this->order,
            'facilities' => FacilityResource::collection($this->facilities)
        ];
    }
}
