<?php

namespace App\Http\Resources;

use App\Models\Facility;
use Illuminate\Http\Resources\Json\JsonResource;

class FacilityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            Facility::FIELD_ID   => $this->id,
            Facility::FIELD_NAME => $this->name
        ];
    }
}
