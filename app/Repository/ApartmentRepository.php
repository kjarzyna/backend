<?php

namespace App\Repository;

use App\Models\Apartment;
use App\Service\QueryBuilder\FilterItem;
use App\Service\QueryBuilder\PaginationItem;
use App\Service\QueryBuilder\Query;
use App\Service\QueryBuilder\QueryValidator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ApartmentRepository
 * @package App\Repository
 */
class ApartmentRepository
{

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     * @throws ValidationException
     */
    public function findByRequestQuery(Request $request): LengthAwarePaginator
    {
        $queryData = json_decode($request->get('q'), true) ?? [];

        QueryValidator::validateQueryParams($queryData);

        $query = new Query(new Apartment());

        foreach ($queryData[Query::KEY_FILTERS] as $filter) {
            $query->addFilter(FilterItem::createFromArray($filter), $query->getQuery());
        }

        $query->addOrder(Apartment::FIELD_ORDER);

        return $query->bindPagination(PaginationItem::createFromArray($queryData[Query::KEY_META]));
    }
}
