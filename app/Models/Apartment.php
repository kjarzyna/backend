<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Apartment
 * @package App\Models
 *
 * @property int $id
 * @property int $hotel_id
 * @property string $photo_url
 * @property int $price
 * @property int $guests_max_amount
 * @property int $room_area
 * @property int $order
 */
class Apartment extends Model
{

    public const TABLE_NAME = 'apartments';

    public const FIELD_ID                   = 'id';
    public const FIELD_HOTEL_ID             = 'hotel_id';
    public const FIELD_NAME                 = 'name';
    public const FIELD_PHOTO_URL            = 'photo_url';
    public const FIELD_PRICE                = 'price';
    public const FIELD_GUESTS_MAX_AMOUNT    = 'guests_max_amount';
    public const FIELD_ROOM_AREA            = 'room_area';
    public const FIELD_ORDER                = 'order';

    use HasFactory;

    /** @var bool $timestamps */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function hotel(): BelongsTo
    {
        return $this->belongsTo(Hotel::class);
    }

    /**
     * @return BelongsToMany
     */
    public function facilities(): BelongsToMany
    {
        return $this->belongsToMany(Facility::class, 'apartment_facility');
    }
}
