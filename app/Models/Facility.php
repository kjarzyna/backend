<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Facility
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 */
class Facility extends Model
{
    public const TABLE_NAME = 'facilities';

    public const FIELD_ID   = 'id';
    public const FIELD_NAME = 'name';

    use HasFactory;

    /** @var bool $timestamps */
    public $timestamps = false;
}
