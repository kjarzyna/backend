<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 */
class City extends Model
{
    public const TABLE_NAME = 'cities';

    public const FIELD_ID   = 'id';
    public const FIELD_NAME = 'name';

    use HasFactory;

    /** @var bool $timestamps */
    public $timestamps = false;
}
