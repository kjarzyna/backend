<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Hotel
 * @package App\Models
 *
 * @property int id
 * @property string name
 */
class Hotel extends Model
{

    public const TABLE_NAME = 'hotels';

    public const FIELD_ID           = 'id';
    public const FIELD_NAME         = 'name';
    public const FIELD_CITY_ID      = 'city_id';
    public const FIELD_PHOTO_URL    = 'photo_url';
    public const FIELD_ORDER        = 'order';

    use HasFactory;

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }
}
