<?php

namespace App\Service\QueryBuilder;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

/**
 * Class Query
 * @package App\Service\QueryBuilder
 */
class Query
{

    public const KEY_FILTERS = 'filters';
    public const KEY_META    = 'meta';

    public const ORDER_ASC  = 'ASC';
    public const ORDER_DESC = 'DESC';

    /** @var Model $model */
    public Model $model;

    /** @var Builder $query */
    public Builder $query;

    /**
     * @param Model $model
     * @return $this
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->query = $model->newQuery();

        return $this;
    }

    /**
     * @param FilterItem $filterItem
     * @param $queryBuilder
     * @return $this
     */
    public function addFilter(FilterItem $filterItem, Builder $queryBuilder): self
    {
        if ($this->isNested($filterItem->getKey())) {
            $this->addNestedFilter($filterItem, $queryBuilder);

            return $this;
        }

        $this->bindFilter($filterItem, $queryBuilder);

        return $this;
    }

    /**
     * @param FilterItem $filterItem
     * @param Builder $queryBuilder
     */
    protected function bindFilter(FilterItem $filterItem, Builder $queryBuilder)
    {
        switch ($filterItem->getType()) {
            case 'in';
                $queryBuilder->whereIn($filterItem->getKey(), $filterItem->getValues()->all());
                break;
            default:
                $queryBuilder->where($filterItem->getKey(), $filterItem->getType(), $filterItem->getValues()->first());
                break;
        }
    }

    /**
     * @param FilterItem $filterItem
     * @param Builder $queryBuilder
     */
    public function addNestedFilter(FilterItem $filterItem, Builder $queryBuilder)
    {
        $relation = Str::before( $filterItem->getKey(), '.');

        $queryBuilder->whereHas($relation, function ($q) use ($relation, $filterItem) {
            $nestedFilter = clone $filterItem;
            $nestedFilter->setKey(Str::replaceFirst(sprintf('%s.', $relation), '', $filterItem->getKey()));
            $this->addFilter($nestedFilter, $q);
        });
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function isNested(string $key): bool
    {
        return Str::contains($key, '.');
    }

    /**
     * @param PaginationItem $paginationItem
     * @return LengthAwarePaginator
     */
    public function bindPagination(PaginationItem $paginationItem): LengthAwarePaginator
    {
        return $this->query->paginate($paginationItem->getPerPage(), ['*'], 'page', $paginationItem->getPage());
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return $this->query;
    }

    /**
     * @param string $column
     * @param string $order
     * @return $this
     */
    public function addOrder(string $column, string $order = self::ORDER_ASC): self
    {
        $this->query->orderBy($column, $order);

        return $this;
    }
}
