<?php

namespace App\Service\QueryBuilder;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

/**
 * Class QueryValidator
 * @package App\Service\QueryBuilder
 */
class QueryValidator
{

    /**
     * @param array $params
     * @throws ValidationException
     */
    public static function validateQueryParams(array $params)
    {
        $rules = [
            Query::KEY_FILTERS => ['present', 'array'],
            Query::KEY_META => ['required', 'array'],
            sprintf('%s.%s', Query::KEY_META, PaginationItem::KEY_PAGE) => ['integer', 'required', 'min:1'],
            sprintf('%s.%s', Query::KEY_META, PaginationItem::KEY_PER_PAGE) => ['integer', 'required', Rule::in([10,25,50])]
        ];
        Validator::make($params, $rules)->validate();

        foreach ($params[Query::KEY_FILTERS] as $filter) {
            $rules = [
                FilterItem::KEY_FIELD => ['required', 'string'],
                FilterItem::KEY_TYPE => ['required', 'string', Rule::in(array_keys(FilterItem::FILTER_TYPE_MAP))],
                FilterItem::KEY_VALUES => ['required', 'array']
            ];

            Validator::make($filter, $rules)->validate();
        }
    }
}
