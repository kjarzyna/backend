<?php

namespace App\Service\QueryBuilder;

/**
 * Class PaginationItem
 * @package App\Service\QueryBuilder
 */
class PaginationItem
{

    public const KEY_PAGE = 'page';
    public const KEY_PER_PAGE = 'perPage';

    /** @var int $page */
    protected int $page;

    /** @var int $perPage */
    protected int $perPage;

    /**
     * @param array $params
     * @return PaginationItem
     */
    public static function createFromArray(array $params): self
    {
        return (new self())
            ->setPage($params[self::KEY_PAGE])
            ->setPerPage($params[self::KEY_PER_PAGE]);
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return PaginationItem
     */
    public function setPage(int $page): PaginationItem
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     * @return PaginationItem
     */
    public function setPerPage(int $perPage): PaginationItem
    {
        $this->perPage = $perPage;
        return $this;
    }
}
