<?php

namespace App\Service\QueryBuilder;

use Illuminate\Support\Collection;

/**
 * Class FilterItem
 * @package App\Service\QueryBuilder
 */
class FilterItem
{

    public const FILTER_TYPE_MAP = [
        'IN' => 'in',
        'GTE'=> '>=',
        'LTE'=> '<='
    ];

    public const KEY_FIELD = 'key';
    public const KEY_TYPE = 'type';
    public const KEY_VALUES = 'values';

    /** @var string $key */
    protected string $key;

    /** @var string $type */
    protected string $type;

    /** @var Collection $values */
    protected Collection $values;

    /**
     * @param array $params
     * @return static
     */
    public static function createFromArray(array $params): self
    {
        return (new self())
            ->setKey($params[self::KEY_FIELD])
            ->setType(self::FILTER_TYPE_MAP[$params[self::KEY_TYPE]])
            ->setValues(new Collection($params[self::KEY_VALUES]));
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return FilterItem
     */
    public function setKey(string $key): FilterItem
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FilterItem
     */
    public function setType(string $type): FilterItem
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    /**
     * @param Collection $values
     * @return FilterItem
     */
    public function setValues(Collection $values): FilterItem
    {
        $this->values = $values;
        return $this;
    }
}
