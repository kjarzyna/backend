<?php

namespace Database\Factories;

use App\Models\Hotel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class HotelFactory extends Factory
{
    public const DEFAULT_ITEMS_COUNT = 7;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Hotel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => sprintf('%s Hotel', ucfirst($this->faker->word)),
            'city_id' => $this->faker->numberBetween(1, CityFactory::DEFAULT_ITEMS_COUNT),
            'order' => $this->faker->numberBetween(100, 100000),
            'photo_url' => 'https://images.unsplash.com/photo-1496417263034-38ec4f0b665a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80'
        ];
    }
}
