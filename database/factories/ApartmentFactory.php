<?php

namespace Database\Factories;

use App\Models\Apartment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ApartmentFactory extends Factory
{

    public const DEFAULT_ITEMS_COUNT = 110;

    protected const AVAILABLE_PHOTOS = [
        'https://images.unsplash.com/photo-1590074072786-a66914d668f1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80',
        'https://images.unsplash.com/photo-1445991842772-097fea258e7b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        'https://images.unsplash.com/photo-1590073242678-70ee3fc28e8e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1308&q=80',
        'https://images.unsplash.com/photo-1584132967334-10e028bd69f7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        'https://images.unsplash.com/photo-1529290130-4ca3753253ae?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80'
    ];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Apartment::class;



    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'hotel_id' => $this->faker->numberBetween(1, HotelFactory::DEFAULT_ITEMS_COUNT),
            'name' => sprintf('%s Apartment', ucfirst($this->faker->word)),
            'photo_url' => self::AVAILABLE_PHOTOS[$this->faker->numberBetween(0, count(self::AVAILABLE_PHOTOS) - 1)],
            'price' => $this->faker->numberBetween(50, 1000),
            'guests_max_amount' => $this->faker->numberBetween(1, 10),
            'room_area' => $this->faker->numberBetween(10, 100),
            'order' => $this->faker->numberBetween(10, 100000),
        ];
    }
}
