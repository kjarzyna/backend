<?php

namespace Database\Seeders;

use App\Models\Apartment;
use App\Models\City;
use App\Models\Facility;
use App\Models\Hotel;
use Database\Factories\ApartmentFactory;
use Database\Factories\CityFactory;
use Database\Factories\FacilityFactory;
use Database\Factories\HotelFactory;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\WithFaker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        City::factory(CityFactory::DEFAULT_ITEMS_COUNT)->create();
        Hotel::factory(HotelFactory::DEFAULT_ITEMS_COUNT)->create();
        Facility::factory(FacilityFactory::DEFAULT_ITEMS_COUNT)->create();
        Apartment::factory(ApartmentFactory::DEFAULT_ITEMS_COUNT)->create()->each(function ($apartment) {
            $facilities = [];
            for ($i = 1; $i <= rand(1, 10); $i++) {
                $facilities[] = rand(1, FacilityFactory::DEFAULT_ITEMS_COUNT);
            }
            $apartment->facilities()->sync($facilities);
        });
    }
}
