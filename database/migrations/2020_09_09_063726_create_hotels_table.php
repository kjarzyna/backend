<?php

use App\Models\City;
use App\Models\Hotel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Hotel::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(Hotel::FIELD_NAME);
            $table->unsignedBigInteger(Hotel::FIELD_CITY_ID);
            $table->string(Hotel::FIELD_PHOTO_URL);
            $table->integer(Hotel::FIELD_ORDER);
        });

        Schema::table(Hotel::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(Hotel::FIELD_CITY_ID)->references(City::FIELD_ID)->on(City::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Hotel::TABLE_NAME);
    }
}
