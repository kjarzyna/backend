<?php

use App\Models\Apartment;
use App\Models\Hotel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Apartment::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(Apartment::FIELD_HOTEL_ID);
            $table->string(Apartment::FIELD_NAME);
            $table->string(Apartment::FIELD_PHOTO_URL);
            $table->integer(Apartment::FIELD_PRICE);
            $table->integer(Apartment::FIELD_GUESTS_MAX_AMOUNT);
            $table->integer(Apartment::FIELD_ROOM_AREA);
            $table->integer(Apartment::FIELD_ORDER);
        });

        Schema::table(Apartment::TABLE_NAME, function (Blueprint $table) {
            $table->foreign(Apartment::FIELD_HOTEL_ID)->references(Hotel::FIELD_ID)->on(Hotel::TABLE_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Apartment::TABLE_NAME);
    }
}
